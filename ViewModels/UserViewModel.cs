using System.ComponentModel.DataAnnotations;

namespace mypos_api.ViewModels
{
    public class UserViewModel
    {
        //data annotation form validate
        [Required]
        public string Username { get; set; }
        [MinLength(1, ErrorMessage = "min8chars")]
        public string Password { get; set; }
    }
}