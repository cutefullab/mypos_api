using System;

namespace mypos_api.Extensions
{
    public static class StringExtension
    {
        //"CodeMobiles".ToBaht() => CodeMobiles Baht
       public static string ToBaht(this String data)
        {
            //logic
            return $"{data} :: Baht";
        }
    }
}