# คำสั่ง ควรเป็นตัวพิมพ์ใหญ่
# โหลด 
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0 AS base 
#WORKDIR  ตั้งต้น
WORKDIR /app
#port
EXPOSE 80
EXPOSE 443

#โหลด sdk
FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build

WORKDIR /src
COPY ["mypos_api.csproj", "./"]
#restore packet
RUN dotnet restore "./mypos_api.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "mypos_api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "mypos_api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "mypos_api.dll"]
